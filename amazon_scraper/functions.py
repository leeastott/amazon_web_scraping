"""
Useful functions.

created Feb. 4/22
@author Leea Stott
"""

from weakref import proxy
from bs4 import BeautifulSoup
from pandas import DataFrame
import requests
import time
import random
import regex as re
import numpy as np
import datetime

""" HEADERS """
# USE MORE SOPHISTICATED HEADERS AND ROTATE BETWEEN THEM TO AVOID BLOCKING
headers_list = [{
    'authority': 'www.amazon.ca',
    'cache-control': 'max-age=0',
    'rtt': '600',
    'downlink': '1.45',
    'ect': '3g',
    'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="98", "Microsoft Edge";v="98"',
    'sec-ch-ua-mobile': '?0',
    'sec-ch-ua-platform': '"Windows"',
    'upgrade-insecure-requests': '1',
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.80 Safari/537.36 Edg/98.0.1108.50',
    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    'service-worker-navigation-preload': 'true',
    'sec-fetch-site': 'same-origin',
    'sec-fetch-mode': 'navigate',
    'sec-fetch-user': '?1',
    'sec-fetch-dest': 'document',
    'accept-language': 'en-US,en;q=0.9',
    'cookie': 's_nr=1567463968687-Repeat; s_vnum=1963184706041%26vn%3D2; sst-acbca=Sst1|PQGddAld0zVCxMcbuBp5kiCDC9Ivpf8n59nZGMpm1IAnLhDvELIWvpsGmhkXhSWPg6vt-geJg4XCqk3sxTSZO2VfXPtJIEUZjRkLNcq9melqVOWhCUx7eRZ4ZiDBnt92JNYMQbdzv3Ivy9T0UeG4KeNCmMrmzrupedf7tDk1xnfpK2xHMh38toVSr0PJIJ8NJKZxIQuS1U54l-deysnTY8LRbGEI_yERWpUvuoapdLoC60QsiLaig-fXM7QZh7ie2fallj2UOiX0Enpj1u-jwkpqtJlxsUXTfBtJSvsFStBdlK3Z344ml7vWffau8ufhosbMK4t3jLf9p7lB4K4JFwxMTA; sess-at-acbca="S2UMermeKknc9qh/7X3TTBvfC2vSOtZPyVv6sK9acrs="; session-id-time=2082787201l; lc-acbca=en_CA; ubid-acbca=132-3248376-2619336; s_dslv=1567463968692; session-id=143-8175402-1124146; at-acbca=Atza|IwEBIKVnZlZgN3Ltt7D884yPA3bugWOVOjL0_hWAXkgIcdDomfAmQ0bwSAC2XkbgrrdRz63CovsOH3spWtGGlx4WAHdhaN25BSiXBN3Fq03P4kKGzAlHAFlgS0voG5--MVHlcqwEccJ75e4Nu5S8KhtL4zPyI3Xf6HRApmOArOgxkecmM5rJgaiRMnn3oHbtSFM1BSIMa1h0gBuP6i1IKgkb1XMW0yqj4DB7rvmSpw_o7bULdU-MO8tqfkVTQgnPiUhBI5Ov7MSg_vMBGajKqtmhOmMme8g1nNECt-PH0k-LaMlbh_JP0R62RXTQc5NcX0l8iP9Y3wECFAK3eNE68N8GgrMwtLDnzYvD1r-qbAxiuvCjvcrGoy8F5xcwoK1KSI689mUGul25J_MkY_3en_MTWbsB; i18n-prefs=CAD; session-token=nr8xN1bjDAfcNFNhqXMO4Pjd5BzwrFwRkZZCgZ25PO3iHKKi9Y40FEhKpswTqjnsOF/3wnJ9gvqD1ddOSPtL9YtGregstb6tq6XIcX9D/snN2DIZD1OPTnCe6sjLhDbA4xsZz0KoH4Atsn4RWogqmkXwm2yHh67bDQaD2BduN+KOSuWF+lHMKYl5WQWQNx/3; csm-hit=tb:5XWF63A4P5MFYD1XGYJR+s-6968ZWVQMCWDP3PBXM5A|1644884316030&t:1644884316030&adb:adblk_no',
},{
    'authority': 'www.amazon.ca',
    'rtt': '250',
    'downlink': '1.4',
    'ect': '4g',
    'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="98", "Microsoft Edge";v="98"',
    'sec-ch-ua-mobile': '?0',
    'sec-ch-ua-platform': '"Windows"',
    'upgrade-insecure-requests': '1',
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.80 Safari/537.36 Edg/98.0.1108.50',
    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    'service-worker-navigation-preload': 'true',
    'sec-fetch-site': 'none',
    'sec-fetch-mode': 'navigate',
    'sec-fetch-user': '?1',
    'sec-fetch-dest': 'document',
    'accept-language': 'en-US,en;q=0.9',
    'cookie': 's_nr=1567463968687-Repeat; s_vnum=1963184706041%26vn%3D2; sst-acbca=Sst1|PQGddAld0zVCxMcbuBp5kiCDC9Ivpf8n59nZGMpm1IAnLhDvELIWvpsGmhkXhSWPg6vt-geJg4XCqk3sxTSZO2VfXPtJIEUZjRkLNcq9melqVOWhCUx7eRZ4ZiDBnt92JNYMQbdzv3Ivy9T0UeG4KeNCmMrmzrupedf7tDk1xnfpK2xHMh38toVSr0PJIJ8NJKZxIQuS1U54l-deysnTY8LRbGEI_yERWpUvuoapdLoC60QsiLaig-fXM7QZh7ie2fallj2UOiX0Enpj1u-jwkpqtJlxsUXTfBtJSvsFStBdlK3Z344ml7vWffau8ufhosbMK4t3jLf9p7lB4K4JFwxMTA; sess-at-acbca="S2UMermeKknc9qh/7X3TTBvfC2vSOtZPyVv6sK9acrs="; session-id-time=2082787201l; lc-acbca=en_CA; ubid-acbca=132-3248376-2619336; s_dslv=1567463968692; session-id=143-8175402-1124146; at-acbca=Atza|IwEBIKVnZlZgN3Ltt7D884yPA3bugWOVOjL0_hWAXkgIcdDomfAmQ0bwSAC2XkbgrrdRz63CovsOH3spWtGGlx4WAHdhaN25BSiXBN3Fq03P4kKGzAlHAFlgS0voG5--MVHlcqwEccJ75e4Nu5S8KhtL4zPyI3Xf6HRApmOArOgxkecmM5rJgaiRMnn3oHbtSFM1BSIMa1h0gBuP6i1IKgkb1XMW0yqj4DB7rvmSpw_o7bULdU-MO8tqfkVTQgnPiUhBI5Ov7MSg_vMBGajKqtmhOmMme8g1nNECt-PH0k-LaMlbh_JP0R62RXTQc5NcX0l8iP9Y3wECFAK3eNE68N8GgrMwtLDnzYvD1r-qbAxiuvCjvcrGoy8F5xcwoK1KSI689mUGul25J_MkY_3en_MTWbsB; i18n-prefs=CAD; session-token=nr8xN1bjDAfcNFNhqXMO4Pjd5BzwrFwRkZZCgZ25PO3iHKKi9Y40FEhKpswTqjnsOF/3wnJ9gvqD1ddOSPtL9YtGregstb6tq6XIcX9D/snN2DIZD1OPTnCe6sjLhDbA4xsZz0KoH4Atsn4RWogqmkXwm2yHh67bDQaD2BduN+KOSuWF+lHMKYl5WQWQNx/3; csm-hit=tb:5XWF63A4P5MFYD1XGYJR+s-WG6BHMSKBC7EFT5B9P8G|1644884800341&t:1644884800342&adb:adblk_no',
}, {
    'authority': 'www.amazon.ca',
    'rtt': '250',
    'downlink': '1.4',
    'ect': '4g',
    'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="98", "Microsoft Edge";v="98"',
    'sec-ch-ua-mobile': '?0',
    'sec-ch-ua-platform': '"Windows"',
    'upgrade-insecure-requests': '1',
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.80 Safari/537.36 Edg/98.0.1108.50',
    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    'service-worker-navigation-preload': 'true',
    'sec-fetch-site': 'none',
    'sec-fetch-mode': 'navigate',
    'sec-fetch-user': '?1',
    'sec-fetch-dest': 'document',
    'accept-language': 'en-US,en;q=0.9',
    'cookie': 's_nr=1567463968687-Repeat; s_vnum=1963184706041%26vn%3D2; sst-acbca=Sst1|PQGddAld0zVCxMcbuBp5kiCDC9Ivpf8n59nZGMpm1IAnLhDvELIWvpsGmhkXhSWPg6vt-geJg4XCqk3sxTSZO2VfXPtJIEUZjRkLNcq9melqVOWhCUx7eRZ4ZiDBnt92JNYMQbdzv3Ivy9T0UeG4KeNCmMrmzrupedf7tDk1xnfpK2xHMh38toVSr0PJIJ8NJKZxIQuS1U54l-deysnTY8LRbGEI_yERWpUvuoapdLoC60QsiLaig-fXM7QZh7ie2fallj2UOiX0Enpj1u-jwkpqtJlxsUXTfBtJSvsFStBdlK3Z344ml7vWffau8ufhosbMK4t3jLf9p7lB4K4JFwxMTA; sess-at-acbca="S2UMermeKknc9qh/7X3TTBvfC2vSOtZPyVv6sK9acrs="; session-id-time=2082787201l; lc-acbca=en_CA; ubid-acbca=132-3248376-2619336; s_dslv=1567463968692; session-id=143-8175402-1124146; at-acbca=Atza|IwEBIKVnZlZgN3Ltt7D884yPA3bugWOVOjL0_hWAXkgIcdDomfAmQ0bwSAC2XkbgrrdRz63CovsOH3spWtGGlx4WAHdhaN25BSiXBN3Fq03P4kKGzAlHAFlgS0voG5--MVHlcqwEccJ75e4Nu5S8KhtL4zPyI3Xf6HRApmOArOgxkecmM5rJgaiRMnn3oHbtSFM1BSIMa1h0gBuP6i1IKgkb1XMW0yqj4DB7rvmSpw_o7bULdU-MO8tqfkVTQgnPiUhBI5Ov7MSg_vMBGajKqtmhOmMme8g1nNECt-PH0k-LaMlbh_JP0R62RXTQc5NcX0l8iP9Y3wECFAK3eNE68N8GgrMwtLDnzYvD1r-qbAxiuvCjvcrGoy8F5xcwoK1KSI689mUGul25J_MkY_3en_MTWbsB; i18n-prefs=CAD; session-token=nr8xN1bjDAfcNFNhqXMO4Pjd5BzwrFwRkZZCgZ25PO3iHKKi9Y40FEhKpswTqjnsOF/3wnJ9gvqD1ddOSPtL9YtGregstb6tq6XIcX9D/snN2DIZD1OPTnCe6sjLhDbA4xsZz0KoH4Atsn4RWogqmkXwm2yHh67bDQaD2BduN+KOSuWF+lHMKYl5WQWQNx/3; csm-hit=tb:5XWF63A4P5MFYD1XGYJR+s-WG6BHMSKBC7EFT5B9P8G|1644884800341&t:1644884800342&adb:adblk_no',
}, {
    'authority': 'www.amazon.ca',
    'cache-control': 'max-age=0',
    'rtt': '200',
    'downlink': '3.45',
    'ect': '4g',
    'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="98", "Google Chrome";v="98"',
    'sec-ch-ua-mobile': '?0',
    'sec-ch-ua-platform': '"Windows"',
    'upgrade-insecure-requests': '1',
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.82 Safari/537.36',
    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    'service-worker-navigation-preload': 'true',
    'sec-fetch-site': 'same-origin',
    'sec-fetch-mode': 'navigate',
    'sec-fetch-user': '?1',
    'sec-fetch-dest': 'document',
    'accept-language': 'en-US,en;q=0.9',
    'cookie': 'session-id=134-9500302-3043122; session-id-time=2082787201l; i18n-prefs=CAD; ubid-acbca=134-7328429-7919136; session-token=nPsZ+52E3QkKK5ct6N+y/ZZK+6BLywNkPNF38YokIhQpylQct0IbXaap5Yfj9LGTD8BkOrvfEmkQ47QHE2gXe8pvjOQDHlnnD+zLu00aUv3Al2DwNzrXDWQYa/3bfVjkj0NsBT38VRTsrO4VQIwkonXsKoh1C7U0pxxD7JEwtOGeADyhqdyL9lCVT1rJhhGr; csm-hit=tb:s-1P9GGMVXCNJYFXQCAM2X|1644885347981&t:1644885350836&adb:adblk_no',
}, {
    'authority': 'www.amazon.ca',
    'cache-control': 'max-age=0',
    'upgrade-insecure-requests': '1',
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.87 Safari/537.36',
    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    'service-worker-navigation-preload': 'true',
    'sec-gpc': '1',
    'sec-fetch-site': 'same-origin',
    'sec-fetch-mode': 'navigate',
    'sec-fetch-user': '?1',
    'sec-fetch-dest': 'document',
    'accept-language': 'en-US,en;q=0.9',
    'cookie': 'session-id=133-8957364-3101125; i18n-prefs=CAD; ubid-acbca=134-2470715-0593626; lc-acbca=en_CA; s_cc=true; s_vnum=2076119328130%26vn%3D1; s_sq=%5B%5BB%5D%5D; s_ppv=52; s_nr=1644119337996-New; s_dslv=1644119337999; session-id-time=2082787201l; session-token=AwpzlqmQsme+oh85YEAibxRoPAQst1hjUIri4zMpI1F/Ah7ucV9LHRLotK+OEaF6PpzTOTTXzGoHJdejHQgeSN6Ylt2wtUG5THA+Khw+nlsEJTLXyXnJ1vXNfoNhMHzKeijy62vltZ4uiAhNKEx5d6yViyoxMszET+jIUguMlsNpaMgVOIvByShT/TDH6xWA; csm-hit=tb:s-RS1B7AHZPM17SDDZWHAQ|1644965621784&t:1644965622874&adb:adblk_no',
}, {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:94.0) Gecko/20100101 Firefox/94.0',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8',
    'Accept-Language': 'en-US,en;q=0.5',
    'Connection': 'keep-alive',
    'Upgrade-Insecure-Requests': '1',
    'Sec-Fetch-Dest': 'document',
    'Sec-Fetch-Mode': 'navigate',
    'Sec-Fetch-Site': 'none',
    'Sec-Fetch-User': '?1',
    'Cache-Control': 'max-age=0',
    'TE': 'trailers',
    'referer': 'https://www.google.ca/'
}]


def get_products_from_search_page(page_list, header_list: list = headers_list):
    """Go through each page of search results for 'face moisturizer' on Amazon and save the html contents to a file named
    'moisutrizer_{page_num}.html'.

    Parameters
    ----------
    page_list : list
        List of page numbers to be saved.
    header_list : list
        List of headers to rotate through in order to avoid amazon blocks.

    Returns
    -------
    failed_urls : list
        A list of urls which had failed requests.
    failed_pages : list
        A list of all the page numbers which had failed requests.
    page_product_details : list
        A list of the total products (and some features) from all the search pages combined.

    Raises
    ------
    Exception
        If the page was not successfully requested.
    """
    # If the page numbers are in consecutive order, amazon might get suspiscious
    random.shuffle(page_list)
    page_product_details = []

    for ii, page_num in enumerate(page_list):
        print(f'Beginning page {page_num}')
        url = f'https://www.amazon.ca/s?k=moisturizer&i=beauty&rh=n%3A6344771011&page={page_num}&crid=DPX5PYT6VTKQ&qid=1645153989&sprefix=moisturizer%2Caps%2C251&ref=sr_pg_{page_num}'
        HEADERS = random.choice(headers_list)

        # Request the html content of the amazon product page. Success when respose is 200.
        response = requests.get(url, headers=HEADERS)

        # Check for failed urls and store them in a list for a second try
        failed_urls = []
        failed_pages = []
        if response.status_code != 200:
            print(f"Status code: {response.status_code}")
            failed_urls.append(url)
            failed_pages.append(page_num)
            raise Exception(f"Failed to link to web page {url}")

        # Save the html contents of the page for later parsing
        with open(f'moisturizer_{page_num}.html', 'w', encoding="utf-8") as f:
            f.write(response.text)

        # Get a list of all the products on the page and their details
        page_product_details += get_product_details(page_num)  # this will be a list of lists

        # Dont make too many requests per minute
        time.sleep(random.randint(4, 7))

        if page_num % 10 == 0:
            time.sleep(random.randint(30, 60))

        print(f'Completed page {page_num}; {ii+1}/{len(page_list)} pages: {int((ii+1)/len(page_list)*100)}% Complete')

    print(f'\nCompleted with {len(failed_pages)} failed requests.')
    return failed_urls, failed_pages, page_product_details


def get_product_details(page_num: int):
    """Get the product name, rating, number of ratings, price and url for products present on a given search page.

    Paremeters
    ----------
    page_num : int
        Search result page number.

    Returns
    -------
    all : list
        List containing a list for each product [product name, rating, number of ratings, price, url]
    """
    # Retrieve the HTML file, no requests need to be made.
    with open(f'moisturizer_{page_num}.html', 'r', encoding="utf-8") as f:
        html_content = f.read()

    # Parse the HTML file
    content = BeautifulSoup(html_content, "html.parser")

    doc = content.find_all('div',
                           attrs={'class': 'sg-col-4-of-12 s-result-item s-asin sg-col-4-of-16 sg-col s-widget-spacing-small sg-col-4-of-20'})
    all = []
    for i, d in enumerate(doc):
        # Get product name
        name = d.find('span', attrs={'class': 'a-size-base-plus a-color-base a-text-normal'})
        name = name.text if name is not None else None

        # Get product rating and number of ratings
        rating = d.find('span', attrs={'class': 'a-icon-alt'})
        rating = float(rating.text.split()[0]) if rating is not None else None
        num_ratings = d.find('span', attrs={'class': 'a-size-base s-underline-text'})
        num_ratings = int(num_ratings.text.replace(',', '')) if num_ratings is not None else 0

        # Get product price
        price_whole = d.find('span', attrs={'class': 'a-price-whole'})
        price_frac = d.find('span', attrs={'class': 'a-price-fraction'})

        if price_frac and price_whole is not None:
            price = float(price_whole.text.replace(',', '')+price_frac.text)
        elif price_whole is not None:
            price = float(price_whole)
        else:
            price = None

        url = d.find('a',
                     attrs={'class': 'a-link-normal s-underline-text s-underline-link-text s-link-style a-text-normal'})['href']
        url = 'http://amazon.ca'+url if url is not None else None

        all.append([name, rating, num_ratings, price, url, page_num])
    return all


def add_feature_columns(df: DataFrame, insert_cols: bool = True):
    """
    Add columns in the dataframe cooresponding to the features retrieved from fetch_product_features.

    Parameters
    ----------
    df: Dataframe
        Dataframe containing products and product urls.
    insert_cols : bool
        When true, insert columns into the dataframe. 

    Returns
    -------
    new_col_names: list
        List of the new column names.

    """
    col_names = ['unit_price', 'unit_price_per', 'Brand', 'Special ingredients', 'Skin type', 'Product benefits', 'Specific uses for product', 'Recommended uses for product', 'Skin tone', 'Sun protection', 'Scent', 'Key ingredients', 'Ingredients']
    new_col_names = []
    for name in col_names:
        name = name.lower().replace(' ', '_')
        if insert_cols:
            df.insert(len(df.columns), name, ' ')
        new_col_names.append(name)
    return new_col_names


def add_features_to_df(df: DataFrame, indices: list, insert_cols: bool = True):
    """
    Add and fill product details into DataFrame.

    Parameters
    ----------
    df : DataFrame
        DataFrame with product urls
    insert_cols : bool
        When true, insert columns into the dataframe.

    Returns
    -------
    failed_inds : list
        All the Dataframe indices which an exception was thrown.
    """
    # Add the required columns to the dataframe
    cols = add_feature_columns(df, insert_cols=insert_cols)

    # df_index = np.array(df.index)
    random.shuffle(indices)
    # Add the features of each product to the dataframe
    failed_inds = []

    # Create a log file
    date = datetime.datetime.now()
    with open(f'scrape_moisture_log_{date.strftime("%Y_%d_%m_%H_%M")}.txt', 'w') as f:
        f.write('\n')

    for ii, ind in enumerate(indices):
        if ii != 0:
            if ii % 10 == 0:
                time.sleep(random.randint(10, 20))
            if ii % 100 == 0:
                time.sleep(random.randint(60, 120))
            if ii % 1000 == 0:
                time.sleep(random.randint(360, 600))

        msg = f'Beginning parsing of index {ind} at time {datetime.datetime.now().strftime("%Y_%d_%m_%H_%M")}.'
        with open(f'scrape_moisture_log_{date.strftime("%Y_%d_%m_%H_%M")}.txt', 'a') as f:
            f.write(msg)
        print(msg)

        try:
            features, default_length, headers, proxies = fetch_product_features(df.url[ind])

            # If 'features' has an extra entry, it is the price
            if len(features) == (default_length + 1):
                price = features.pop(-1)
                df['price'][ind] = price

            # Add each feature to the DataFrame
            for i, feature in enumerate(features):
                df[cols[i]][ind] = feature

            msg = f'''index {ind} complete. {ii+1} out of {len(indices)} complete  
                      at time {datetime.datetime.now().strftime("%Y_%d_%m_%H_%M")} \n'''

            with open(f'scrape_moisture_log_{date.strftime("%Y_%d_%m_%H_%M")}.txt', 'a') as f:
                f.write(msg)
            print(msg)

        except Exception as e:
            msg = f'''The following exception occured: {e}; at time {datetime.datetime.now().strftime("%Y_%d_%m_%H_%M")} 
            with headers {headers} and proxies {proxies} \n\n'''

            with open(f'scrape_moisture_log_{date.strftime("%Y_%d_%m_%H_%M")}.txt', 'a') as f:
                f.write(msg)
            print(msg)

            failed_inds.append(ind)

            continue

    return failed_inds


def fetch_product_features(url: str, get_price: bool = False):
    """
    adapted from: https://jovian.ai/landryroni/amazon-best-seller-web-scraping

    Make request with given url and parese for product additional product features.

    Parameters
    ----------
    url : str
        facial moisturizer url to download an parse
    get_price : bool
        Retrive the items full price from the product page

    Returns
    -------
    feature_list: list
        A list of length 14 if get_price else length 13 of form:
        ['unit_price', 'unit_price_per', 'Brand', 'Special ingredients', 'Skin type',
            'Product benefits', 'Specific uses for product', 'Recommended uses for product',
            'Skin tone', 'Sun protection', 'Scent', 'Key ingredients', 'Ingredients', Optional[price] if get_price]
    default_length : int
        The length of features_list if get_price is default value False


    """
    try:
        proxies_list = ["128.199.109.241:8080", "113.53.230.195:3128", "125.141.200.53:80", "125.141.200.14:80",
                        "128.199.200.112:138", "149.56.123.99:3128", "128.199.200.112:80", "125.141.200.39:80",
                        "134.213.29.202:4444"]
        proxies = {'https://': random.choice(proxies_list)}
        headers = random.choice(headers_list)
        response = requests.get(url, headers=headers, proxies=proxies)

        feature_list = []
        default_length = 0

        # https://stackoverflow.com/questions/52267189/python-web-automation-http-requests-or-headless-browser/52273210
        # cookies = response.cookies.get_dict()

        if response.status_code != 200:
            print(f"Status code: {response.status_code}")
            raise Exception(f"Failed to link to web page {url}")

        page_content = BeautifulSoup(response.text, "html.parser")

        # TODO ADD IF NONE's TO THESE
        # Get the unit price
        # price_header = page_content.find('td', attrs={'class': 'a-color-secondary a-size-base a-text-right a-nowrap'},
        # text="Price:")
        price_header = page_content.find('span', attrs={'class': 'a-size-mini a-color-base aok-align-center pricePerUnit'})
        if price_header is not None:
            unit_price = price_header.find('span', attrs={'class': 'a-offscreen'}).text.strip('$')
            per_label = price_header.contents[-1].strip(' /)')
        else:
            unit_price = None
            per_label = None

        feature_list.append(unit_price)
        feature_list.append(per_label)

        # Get the whole product overview
        prod_overview = page_content.find('div', attrs={'class': 'celwidget', 'id': 'productOverview_feature_div'})

        # Get the features from the product overview
        tags = ['Brand', 'Special ingredients', 'Skin type', 'Product benefits', 'Specific uses for product',
                'Recommended uses for product', 'Skin tone', 'Sun protection', 'Scent']
        for tag in tags:
            label = prod_overview.find('span', attrs={'class', "a-size-base a-text-bold"}, text=tag)
            if label is not None:
                feature = label.find_next('td').text.strip()
            else:
                feature = None
            feature_list.append(feature)

        # Now for the entire ingredient list residing in the important infromation section
        imp_info = page_content.find('div', id='important-information')
        # Sometimes some ingredients are listed in teh 'KEY INGREDIENTS' section intead
        # key_ings = page_content.find('span', attrs={'class', 'a-color-secondary'},
        # text=(lambda x: x.text.lower == ' key ingredients '))
        key_ings_label = page_content.find('span', attrs={'class', 'a-color-secondary'},
                                        text=re.compile(' Key Ingredients ', re.IGNORECASE))

        if imp_info is not None:
            ings = imp_info.find('h4', text='Ingredients')
            if ings is not None:
                ings_list = ings.find_next_sibling('p').text
            else:
                ings_list = None
        else:
            ings_list = None
        if key_ings_label is not None:
            key_ings_class = key_ings_label.find_next('td', attrs={'class', 'apm-tablemodule-valuecell selected'})
            if key_ings_class is not None:
                key_ings = key_ings_class.text.strip()
        else:
            key_ings = None
        feature_list.append(key_ings)
        feature_list.append(ings_list)

        default_length = len(feature_list)

        if get_price:
            # Get product price
            price_whole = page_content.find('span', attrs={'class': 'a-price-whole'})
            price_frac = page_content.find('span', attrs={'class': 'a-price-fraction'})

            if price_frac and price_whole is not None:
                price = float(price_whole.text.replace(',', '')+price_frac.text)
            elif price_whole is not None:
                price = float(price_whole)
            else:
                price = None
            feature_list.append(price)
        # Dont make too many requests per minute
        time.sleep(random.randint(4, 7))
    except Exception as e:
        print(f'The following exception occured: {e}')
    finally:
        return feature_list, default_length, headers, proxies
